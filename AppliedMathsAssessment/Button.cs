﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace AppliedMathsAssessment
{
    class Button : Sprite
    {
        // ---------------------------
        // Data
        // ---------------------------
        SpriteFont font;
        Texture2D buttonTexture;
        Texture2D pressedTexture;
        string buttonText;
        Color color = Color.White;
        bool heldDown = false;
        SoundEffect clickSFX;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Button(Texture2D newButtonTexture, Texture2D newPressedTexture, SpriteFont newButtonFont, SoundEffect newClickSFX)
            : base(newButtonTexture)
        {
            buttonTexture = newButtonTexture;
            pressedTexture = newPressedTexture;
            font = newButtonFont;
            clickSFX = newClickSFX;
        }
        // ---------------------------
        public void SetString(string newString)
        {
            buttonText = newString;
        }
        // ---------------------------
        public void SetColor(Color newColor)
        {
            color = newColor;
        }
        // ---------------------------
        public bool IsClicked()
        {
            bool clicked = false;

            MouseState state = Mouse.GetState();
            
            // Is the mouse over the button?
            if (GetBounds().Contains(state.Position))
            {
                // The mouse is inside the button area
                // Check if it is also being held down
                if (state.LeftButton == ButtonState.Pressed)
                {
                    // click is being held down in the button area
                    // Update the held flag and change our texture to pressed
                    heldDown = true;
                    texture = pressedTexture;
                }
                else
                {
                    // click is not held down, but the mouse is in the button area
                    // check if we previously had the button held down
                    if (heldDown == true)
                    {
                        // We did previously have the button held in this area, so this is a click!
                        clicked = true;
                        clickSFX.Play();
                    }

                    // Either way we should go back to our normal flag and texture
                    heldDown = false;
                    texture = buttonTexture;
                }
            }
            else
            {
                // Mouse is outside of the button area, so we aren't holding this button down
                // Change held flag and change the texture back to normal
                heldDown = false;
                texture = buttonTexture;
            }

            return clicked;
        }
        // ---------------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch); // draw the button image

            // Adjust positoin of text for center of butotn
            Vector2 adjustedPosition = position;
            adjustedPosition.X += GetBounds().Width / 2;
            adjustedPosition.Y += GetBounds().Height / 2;

            // Adjust allignment of text to senter aligned
            Vector2 textSize = font.MeasureString(buttonText);
            adjustedPosition.X -= textSize.X / 2;
            adjustedPosition.Y -= textSize.Y / 2;

            // Draw Text
            spriteBatch.DrawString(font, buttonText, adjustedPosition, color);
        }
        // ---------------------------

    }
}
