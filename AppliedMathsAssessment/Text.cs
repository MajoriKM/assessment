﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    class Text
    {
        // ------------------
        // Enums
        // ------------------
        public enum TextAlignment
        {
            TOP_LEFT,
            TOP,
            TOP_RIGHT,
            LEFT,
            CENTRE,
            RIGHT,
            BOTTOM_LEFT,
            BOTTOM,
            BOTTOM_RIGHT
        }


        // ------------------
        // Data
        // ------------------
        SpriteFont font;
        string textString;
        Vector2 position;
        Color color = Color.White;
        TextAlignment alignment;


        // ------------------
        // Behaviour
        // ------------------
        public Text(SpriteFont newFont)
        {
            font = newFont;
        }
        // ------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 adjustedPosition = position;
            Vector2 textSize = font.MeasureString(textString);

            switch(alignment)
            {
                case TextAlignment.TOP_LEFT:
                    // Do nothing, already alligned this way by default
                    break;
                case TextAlignment.TOP:
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case TextAlignment.TOP_RIGHT:
                    adjustedPosition.X -= textSize.X;
                    break;
                case TextAlignment.LEFT:
                    adjustedPosition.Y -= textSize.Y / 2;
                    break;
                case TextAlignment.CENTRE:
                    adjustedPosition.Y -= textSize.Y / 2;
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case TextAlignment.RIGHT:
                    adjustedPosition.Y -= textSize.Y / 2;
                    adjustedPosition.X -= textSize.X;
                    break;
                case TextAlignment.BOTTOM_LEFT:
                    adjustedPosition.Y -= textSize.Y;
                    break;
                case TextAlignment.BOTTOM:
                    adjustedPosition.Y -= textSize.Y;
                    adjustedPosition.X -= textSize.X / 2;
                    break;
                case TextAlignment.BOTTOM_RIGHT:
                    adjustedPosition.Y -= textSize.Y;
                    adjustedPosition.X -= textSize.X;
                    break;
            }

            spriteBatch.DrawString(font, textString, adjustedPosition, color);
        }
        // ------------------
        public void SetTextString(string newString)
        {
            textString = newString;
        }
        // ------------------
        public void SetColor(Color newColor)
        {
            color = newColor;
        }
        // ------------------
        public void SetAlignment(TextAlignment newAlignment)
        {
            alignment = newAlignment;
        }
        // ------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        // ------------------
    }
}
